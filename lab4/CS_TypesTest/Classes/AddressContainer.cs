using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class AddressContainer
	{
		protected string address;

		public AddressContainer() { }

		public AddressContainer(string address)
		{
			this.address = address;
		}

		public static void SwapAddresses(
			ref AddressContainer ac1,
			ref AddressContainer ac2)
		{
			string temp = ac1.address;
			ac1.address = ac2.address;
			ac2.address = temp;
		}
	}
}
