using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class Bird: IFlying
	{
		public void Fly() => Console.WriteLine("I can fly!");
	}
}
