using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class InnerClassAccess
	{
		private class PrivateInnerClass
		{
			public int integer;
		}

		protected class ProtectedInnerClass
		{
			public string str;
		}
	}
}
