using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class StructTester
	{
		Stopwatch _stopwatch;
		public StructTester()
		{
			_stopwatch = new Stopwatch();
		}
		public void StartTest()
		{
			int i;
			for (i = 1; i < 10_000_000; ++i)
			{

				_stopwatch.Start();
				using (StructPerfomanceTest spt = new StructPerfomanceTest()) ;
				_stopwatch.Stop();
				if (i % 1_000_000 == 0)
				{
					Console.WriteLine($"{i} = {_stopwatch.Elapsed.TotalMilliseconds} ms");
				}
			}
			Console.WriteLine($"{i} = {_stopwatch.Elapsed.TotalMilliseconds} ms");
		}
	}
}
