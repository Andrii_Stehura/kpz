using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class House: AddressContainer, IAddressProvider
	{
		private static int _houseCount;
		private int _id;

		static House()
		{
			_houseCount = 0;
		}

		public House(string address): base(address) 
		{
			Console.WriteLine(this.address); //base.address
			_id = ++_houseCount;
		}

		public House(string address, int id)
		{
			base.address = address;
			_id = id;
			++_houseCount;
		}

		public string GetAddress() => address;
		public string GetAddress(string zipCode) => $"{address}, {zipCode}";

		public static void CopyAddressTo(House h1, out House h2)
		{
			h2 = new House(h1.address);
		}

		public void SetId(object id)
		{
			_id = (int)id;
		}

		public static implicit operator House(string str) => new House(str);
		public static explicit operator string(House h) => h.address;
	}
}
