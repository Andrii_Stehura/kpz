using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class Pegasus : IRunning, IFlying
	{
		public void Fly() => Console.WriteLine("I am flying!");

		public void Run() => Console.WriteLine("I am running!");
	}
}
