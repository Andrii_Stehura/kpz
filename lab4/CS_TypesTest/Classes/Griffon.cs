using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Classes
{
	class Griffon : Bird, IRunning
	{
		public void Run() => Console.WriteLine("I am running and I am very dangerous!");
	}
}
