using lab4.Classes;
using lab4.Enums;
using System;

namespace lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //EnumTest();
            //_ = new House("sasd");
            StructTester st = new StructTester();
            st.StartTest();
        }

        /*static void HouseAccessTest()
        {
            House h = new House("address");
            h._id = 5;
            AddressContainer ac = h;
            ac.address = "another address";
        }*/

        /*static void DefaultAccessTest()
        {
            DefaultAccessTestClass obj = new DefaultAccessTestClass();
            obj.str = "some string";
            var inner = new DefaultAccessTestClass.Inner();

            StructAccessTest structure = new StructAccessTest();
            structure.SomeMethod();
            structure.bigNumber++;
            var structInner = new StructAccessTest.Inner();
        }*/

        private static void EnumTest()
        {
            Enumeration e1 = Enumeration.Human;
            Enumeration e2 = Enumeration.Horse;
            Enumeration e3 = Enumeration.Centaur;
            Console.WriteLine(e1 | e2);
            Console.WriteLine(e2 & e3);
            Console.WriteLine(e3 ^ e2);
        }

        private static void OperatorsTest()
        {
            House h = "address";//implicit
            string str = (string)h;//explicit
        }
    }
}
