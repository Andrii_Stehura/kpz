// CppStructPerfomanceTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include "StructPerfomanceTest.h"
#include <chrono>
using namespace std;
using namespace std::chrono;

int main()
{
	double elapsedTime = 0;
	int i;
	for (i = 1; i < 10000000; ++i)
	{
		 time_point<system_clock> timeStart = system_clock::now();
		 StructPerfomanceTest* structure = new StructPerfomanceTest();
		 delete(structure);
		 time_point<system_clock> timeEnd = system_clock::now();
		 elapsedTime += duration_cast<milliseconds>(timeEnd - timeStart).count();
		 if (i % 1000000 == 0)
			 cout << i << " = " << elapsedTime << " ms" << endl;
	}
	cout << i << " = " << elapsedTime << " ms" << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
