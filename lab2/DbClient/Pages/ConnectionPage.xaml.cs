﻿using DbClient.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Threading.Tasks;
using DbClient.Windows;
using System.ComponentModel;
using System.Timers;

namespace DbClient.Pages
{
	/// <summary>
	/// Логика взаимодействия для ConnectionPage.xaml
	/// </summary>
	public partial class ConnectionPage : Page, INotifyPropertyChanged
	{
		private delegate void ProgressBarEventHandler(ProgressBarDialog pbd);
		private event ProgressBarEventHandler OnProgressBarShown;
		private event ProgressBarEventHandler OnProgressBarFilled;
		public event PropertyChangedEventHandler PropertyChanged;
		private double progressValue;

		public ConnectionPage()
		{
			InitializeComponent();
			OnProgressBarShown += FillProgressBar;
		}

		public double ProgressBarValue 
		{
			get => progressValue;
			set
			{
				progressValue = value;
				PropertyChanged(this, new PropertyChangedEventArgs("ProgressBarValue"));
			}
		}

		public ICommand ConnectCommand => new RelayCommand((x) =>
		{
			ProgressBarDialog pbd = new ProgressBarDialog();
			pbd.Owner = Application.Current.MainWindow;
			pbd.DataContext = this;
			pbd.ShowInTaskbar = false;
			pbd.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			pbd.Focus();
			pbd.ShowActivated = true;
			pbd.Show();
			OnProgressBarShown?.Invoke(pbd);
		},(x)=>
		{
			if(string.IsNullOrEmpty(ConnectionTextBox.Text))
				return false;
			return true;
		});

		public void FillProgressBar(ProgressBarDialog pbd)
		{
			OnProgressBarFilled += HideProgressBarDialog;
			var timer = new System.Timers.Timer(100);
			timer.Elapsed += (x, y) =>
			{
				if (ProgressBarValue < 100)
				{
					ProgressBarValue += 1;
				}
				else
				{
					timer.Stop();
					OnProgressBarFilled?.Invoke(pbd);
				}
			};
			timer.Start();
		}

		public void HideProgressBarDialog(ProgressBarDialog pbd)
		{
			MessageBox.Show("Success");
			pbd.Dispatcher.Invoke(() =>
			{
				pbd.Owner.Activate();
				pbd.Close();
			});
			OnProgressBarFilled -= this.HideProgressBarDialog;
			ProgressBarValue = 0;
		}
	}
}
