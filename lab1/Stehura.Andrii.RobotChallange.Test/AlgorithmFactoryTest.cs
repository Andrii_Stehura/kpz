﻿using NUnit.Framework;
using Robot.Common;
using StehuraAndrii.RobotChallange.Algorithms;
using System;
using System.Collections.Generic;
using System.Text;
using StehuraAndrii.RobotChallange;
using StehuraAndrii.RobotChallange.Factories;

namespace Stehura.Andrii.RobotChallange.Test
{
	[TestFixture]
	class AlgorithmFactoryTest
	{
		private IList<Robot.Common.Robot> _robots;
		private int _index;
		private Map _map;
		private IRobotAlgorithm algorithm;
		IAlgorithmFactory _factory;

		[SetUp]
		public void Setup()
		{
			_robots = new List<Robot.Common.Robot>();
			_index = 0;
			_map = new Map();
			_map.MaxPozition = new Position(100, 100);
			_map.MinPozition = new Position(0, 0);
			_map.Stations = new List<EnergyStation>();
			algorithm = new StehuraAndriiAlgorithm();
			_factory = new AlgorithmFactory();

			_robots.Add(new Robot.Common.Robot()
			{
				Position = new Position(0, 0),
				Energy = 100,
				OwnerName = algorithm.Author
			});

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 100,
				Position = new Position(10, 10),
				RecoveryRate = 20
			});

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 150,
				Position = new Position(10, 15),
				RecoveryRate = 25
			});
		}

		[Test]
		public void GetBreedAlgorithm()
		{
			_robots[0].Energy = 1200;
			Assert.IsInstanceOf(typeof(BreedAlgorithm), _factory.GetAlgorithm(_robots, 0, _map));
		}
	}
}
