using NUnit.Framework;
using Robot.Common;
using System.Collections.Generic;
using Stehura.Andrii.RobotChallange;
using StehuraAndrii.RobotChallange.Algorithms;
using StehuraAndrii.RobotChallange.Helpers;

namespace Stehura.Andrii.RobotChallange.Test
{
	public class HelpersTests
	{
		private IList<Robot.Common.Robot> _robots;
		private int _index;
		private Map _map;
		private IRobotAlgorithm algorithm;

		[SetUp]
		public void Setup()
		{
			_robots = new List<Robot.Common.Robot>();
			_index = 0;
			_map = new Map();
			_map.MaxPozition = new Position(100, 100);
			_map.MinPozition = new Position(0, 0);
			_map.Stations = new List<EnergyStation>();
			algorithm = new StehuraAndriiAlgorithm();

			_robots.Add(new Robot.Common.Robot()
			{
				Position = new Position(0, 0),
				Energy = 100,
				OwnerName = algorithm.Author
			});

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 100,
				Position = new Position(10, 10),
				RecoveryRate = 20
			});

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 150,
				Position = new Position(10, 15),
				RecoveryRate = 25
			});
		}

		[Test]
		public void DistanceHelperTest()
		{
			int res = DistanceHelper.FindDistance(_robots[_index].Position, _map.Stations[0].Position);
			Assert.Greater(res, 0);
		}

		[Test]
		public void StationHelper_FindNearestStation_Test()
		{
			var station = StationHelper.FindNearestStation(_robots[_index], _map);
			Assert.AreEqual(station, _map.Stations[0]);
		}

		[Test]
		public void StationHelper_GetCollectablePositions()
		{
			var positions = StationHelper.GetCollectablePositions(_map.Stations[0], _map);
			Assert.IsNotEmpty(positions);
			Assert.AreEqual(13, positions.Count);
		}
	}
}