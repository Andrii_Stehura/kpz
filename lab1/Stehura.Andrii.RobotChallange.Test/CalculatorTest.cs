﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Robot.Common;
using StehuraAndrii.RobotChallange.Algorithms;
using StehuraAndrii.RobotChallange.Calculators;
using StehuraAndrii.RobotChallange.Helpers;

namespace Stehura.Andrii.RobotChallange.Test
{
	[TestFixture]
	class CalculatorTest
	{
		private IList<Robot.Common.Robot> _robots;
		private int _index;
		private Map _map;
		private IRobotAlgorithm _algorithm;
		private List<int> _ownedIndexes;

		[SetUp]
		public void Setup()
		{
			_robots = new List<Robot.Common.Robot>();
			_index = 0;
			_map = new Map();
			_map.MaxPozition = new Position(100, 100);
			_map.MinPozition = new Position(0, 0);
			_map.Stations = new List<EnergyStation>();
			_algorithm = new StehuraAndriiAlgorithm();
			_ownedIndexes = new List<int>();

			_robots.Add(new Robot.Common.Robot()
			{
				Position = new Position(0, 0),
				Energy = 100,
				OwnerName = _algorithm.Author
			});

			_ownedIndexes.Add(0);

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 100,
				Position = new Position(10, 10),
				RecoveryRate = 20
			});

			_map.Stations.Add(new EnergyStation()
			{
				Energy = 150,
				Position = new Position(10, 15),
				RecoveryRate = 25
			});
		}

		[Test]
		public void GetAutoritativeRobot_NullTest()
		{
			var calc = new AutoritativeCalculator(_robots, _index, _map, _ownedIndexes);
			Assert.IsNull(calc.GetAutoritativeRobot());
		}

		[Test]
		public void GetAutoritativeRobot_NotNullTest()
		{
			_robots.Add(new Robot.Common.Robot() { Position = new Position(15, 15) });
			var calc = new AutoritativeCalculator(_robots, _index, _map, _ownedIndexes);
			Assert.IsNotNull(calc.GetAutoritativeRobot());
		}

		[Test]
		public void HasCollectableFreePositions_True()
		{
			var calc = new AutoritativeCalculator(_robots, _index, _map, _ownedIndexes);
			Assert.IsTrue(calc.HasCollectableFreePositions(_map.Stations[0]));
		}

		[Test]
		public void HasCollectableFreePositions_False()
		{
			var positions = StationHelper.GetCollectablePositions(_map.Stations[0], _map);
			foreach (var p in positions)
			{
				_robots.Add(new Robot.Common.Robot() { Position = p, OwnerName = _algorithm.Author });
			}
			var calc = new AutoritativeCalculator(_robots, _index, _map, _ownedIndexes);
			Assert.IsFalse(calc.HasCollectableFreePositions(_map.Stations[0]));
		}
	}
}
