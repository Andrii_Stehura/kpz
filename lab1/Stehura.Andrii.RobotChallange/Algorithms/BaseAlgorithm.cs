﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace StehuraAndrii.RobotChallange.Algorithms
{
	public abstract class BaseAlgorithm
	{
		protected IList<Robot.Common.Robot> _robots;
		protected int _index;
		protected Map _map;

		public BaseAlgorithm(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
		{
			_robots = robots;
			_index = robotToMoveIndex;
			_map = map;
		}

		public abstract RobotCommand GetAlgorithmCommand();
	}
}
