﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace StehuraAndrii.RobotChallange.Algorithms
{
	public class BreedAlgorithm : BaseAlgorithm
	{
		public BreedAlgorithm(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map) 
			: base(robots, robotToMoveIndex, map) { }

		public override RobotCommand GetAlgorithmCommand()
		{
			return new CreateNewRobotCommand();
		}

		static public bool CanRobotBreed(Robot.Common.Robot robot)
		{
			if (robot.Energy > Other.VariantInformation.EnergyToBreed * 1.1)
				return true;
			return false;
		}
	}
}
