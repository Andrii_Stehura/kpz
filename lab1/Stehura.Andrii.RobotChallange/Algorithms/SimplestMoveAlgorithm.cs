﻿using Robot.Common;

namespace StehuraAndrii.RobotChallange.Algorithms
{
	internal class SimplestMoveAlgorithm : BaseAlgorithm
	{
		Position _position;
		public SimplestMoveAlgorithm(Position pos) : base(null, 0, null) 
		{
			_position = pos;
		}

		public override RobotCommand GetAlgorithmCommand()
		{
			return new MoveCommand() { NewPosition = _position };
		}
	}
}
