﻿using Robot.Common;
using System;
using System.Collections.Generic;

namespace StehuraAndrii.RobotChallange.Algorithms
{
	internal class EnergyAlgorithm : BaseAlgorithm
	{
		public EnergyAlgorithm(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map) 
			: base(robots, robotToMoveIndex, map) { }

		public override RobotCommand GetAlgorithmCommand()
		{
			return new CollectEnergyCommand();
		}

		public static bool CanCollectEnergy(Robot.Common.Robot robot, EnergyStation station, Map map)
		{
			return (station.Energy >= station.RecoveryRate * 1.5 && 
				Helpers.StationHelper.GetCollectablePositions(station, map)
					.Contains(robot.Position));
		}
	}
}
