﻿using Robot.Common;
using StehuraAndrii.RobotChallange.Algorithms;
using StehuraAndrii.RobotChallange.Factories;
using StehuraAndrii.RobotChallange.Other;
using System;
using System.Collections.Generic;

namespace StehuraAndrii.RobotChallange.Algorithms
{
	public class StehuraAndriiAlgorithm : Robot.Common.IRobotAlgorithm
	{
		private IAlgorithmFactory _algorithmFactory;

		public string Author => "Stehura Andrii";

		public StehuraAndriiAlgorithm()
		{
			VariantInformation.Author = Author;
			_algorithmFactory = new AlgorithmFactory();
		}

		public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
		{
			BaseAlgorithm algorithm = _algorithmFactory.GetAlgorithm(robots, robotToMoveIndex, map);
			return algorithm.GetAlgorithmCommand();
		}
	}
}
