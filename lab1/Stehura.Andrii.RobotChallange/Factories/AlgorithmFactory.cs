﻿using Robot.Common;
using StehuraAndrii.RobotChallange.Algorithms;
using StehuraAndrii.RobotChallange.Calculators;
using StehuraAndrii.RobotChallange.Other;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StehuraAndrii.RobotChallange.Factories
{
	public class AlgorithmFactory : IAlgorithmFactory
	{
		private List<int> _ownedIndexes;
		public AlgorithmFactory()
		{
			_ownedIndexes = new List<int>();
		}

		public BaseAlgorithm GetAlgorithm(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
		{
			if (_ownedIndexes.Count == 0)
			{
				_ownedIndexes.AddRange(robots.Where((r) => r.OwnerName == robots[robotToMoveIndex].OwnerName)
				.Select((ro) => robots.IndexOf(ro)));
			}
			else if (!_ownedIndexes.Contains(robotToMoveIndex))
				_ownedIndexes.Add(robotToMoveIndex);

			var robot = robots[robotToMoveIndex];
			if (robot.Energy < 10)
				return new EnergyAlgorithm(robots, robotToMoveIndex, map);

			if (_ownedIndexes.Count < 100 && BreedAlgorithm.CanRobotBreed(robot))
				return new BreedAlgorithm(robots, robotToMoveIndex, map);

			var stationCalc = new AutoritativeCalculator(robots, robotToMoveIndex, map, _ownedIndexes);
			EnergyStation station = stationCalc.GetAutoritativeStation();
			if (EnergyAlgorithm.CanCollectEnergy(robot, station, map))
				return new EnergyAlgorithm(robots, robotToMoveIndex, map);

			var pos = stationCalc.GetAutoritativeCollectablePosition(station);
			var enemy = stationCalc.GetAutoritativeRobot();
			int stationProfit = -Helpers.DistanceHelper.FindDistance(
					pos, robot.Position);

			stationProfit += station.Energy + station.RecoveryRate;
			int enemyDistance = Helpers.DistanceHelper.FindDistance(
					enemy.Position, robot.Position);

			int attackProfit = (int)(enemy.Energy * VariantInformation.StealPercentage);
			attackProfit -= enemyDistance + VariantInformation.AttackEnergyWaste;

			if (stationProfit <= 0 && attackProfit <= 0)
				return new EnergyAlgorithm(robots, robotToMoveIndex, map);

			if (stationProfit < attackProfit)
				pos = enemy.Position;
			else if (pos == robot.Position)
				return new EnergyAlgorithm(robots, robotToMoveIndex, map);
			
			return new SimplestMoveAlgorithm(pos);
		}
	}
}
