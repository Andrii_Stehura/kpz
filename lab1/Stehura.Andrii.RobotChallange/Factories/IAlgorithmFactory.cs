﻿using Robot.Common;
using System.Collections.Generic;

namespace StehuraAndrii.RobotChallange.Factories
{
	public interface IAlgorithmFactory
	{
		Algorithms.BaseAlgorithm GetAlgorithm(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map);
	}
}
