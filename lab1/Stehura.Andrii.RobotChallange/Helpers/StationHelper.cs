﻿using System.Collections.Generic;
using System.Linq;
using Robot.Common;

namespace StehuraAndrii.RobotChallange.Helpers
{
	public static class StationHelper
	{
		static public EnergyStation FindNearestStation(Robot.Common.Robot movingRobot, Map map)
		{
			EnergyStation nearest = null;
			int minDistance = int.MaxValue;
			foreach (var station in map.Stations)
			{
				int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

				if (d < minDistance)
				{
					minDistance = d;
					nearest = station;
				}
			}
			return nearest;
		}

		static public List<Position> GetCollectablePositions(EnergyStation station, Map map)
		{
			List<Position> list = new List<Position>();

			int area = Other.VariantInformation.CollectableArea;
			for (int x = station.Position.X - area; x <= station.Position.X; ++x)
			{
				for (int y = station.Position.Y - area; y <= station.Position.Y + area; ++y)
				{
					Position p = new Position(x, y);
					if (DistanceHelper.FindDistance(p, station.Position) <= area)
					{
						list.Add(p.Copy());
						int dx = station.Position.X - x;
						p = new Position(station.Position.X + dx, y);
						if (!list.Contains(p))
							list.Add(p.Copy());
					}
				}
			}
			
			return list.Where((x) => map.IsValid(x)).ToList();
		}
	}
}
