﻿using System;
using System.Linq;
using Robot.Common;


namespace StehuraAndrii.RobotChallange.Helpers
{
	public static class DistanceHelper
	{

        private static int Min2D(int x1, int x2)
        {
            int[] nums =
            {
                (int) Math.Pow(x1 - x2, 2),
                (int) Math.Pow(x1 - x2 + 100, 2),
                (int) Math.Pow(x1 - x2 - 100, 2)
            };
            return nums.Min();
        }

        public static int FindDistance(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }
    }
}
