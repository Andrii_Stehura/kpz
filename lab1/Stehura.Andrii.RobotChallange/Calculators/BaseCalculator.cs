﻿using Robot.Common;
using StehuraAndrii.RobotChallange.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using StehuraAndrii.RobotChallange.Other;

namespace StehuraAndrii.RobotChallange.Calculators
{
	public abstract class BaseCalculator
	{
		protected IList<Robot.Common.Robot> _robots;
		protected int _index;
		protected Map _map;
		
		public BaseCalculator(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
		{
			_robots = robots;
			_index = robotToMoveIndex;
			_map = map;
		}

		public abstract EnergyStation GetAutoritativeStation();

		public abstract bool HasCollectableFreePositions(EnergyStation station);
	}
}
