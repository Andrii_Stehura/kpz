﻿using Robot.Common;
using StehuraAndrii.RobotChallange.Algorithms;
using StehuraAndrii.RobotChallange.Helpers;
using StehuraAndrii.RobotChallange.Other;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StehuraAndrii.RobotChallange.Calculators
{
	public class AutoritativeCalculator : BaseCalculator
	{
		private List<Robot.Common.Robot> _ownedRobots;
		public AutoritativeCalculator(IList<Robot.Common.Robot> robots, int robotToMoveIndex,
			Map map, List<int> ownedIndexes) : base(robots, robotToMoveIndex, map)
		{
			_ownedRobots = new List<Robot.Common.Robot>();
			foreach (var i in ownedIndexes)
				_ownedRobots.Add(robots[i]);
		}

		public Position GetAutoritativeCollectablePosition(EnergyStation station)
		{
			var positions = StationHelper.GetCollectablePositions(station, _map);
			positions.RemoveAll((x) => _robots.Any((y) => x == y.Position && y != _robots[_index]));

			if (positions.Contains(_robots[_index].Position))
				return _robots[_index].Position;

			if (positions.Contains(station.Position))
				return station.Position;

			if (positions.Count > 0)
				return positions.OrderBy((x) => DistanceHelper.FindDistance(
					x, _robots[_index].Position)).First();

			return station.Position;
		}

		public override EnergyStation GetAutoritativeStation()
		{
			var robot = _robots[_index];

			IEnumerable<EnergyStation> stations = _map.GetNearbyResources(
				robot.Position, VariantInformation.CollectableArea);
			if(stations.Count((x)=> EnergyAlgorithm.CanCollectEnergy(robot, x, _map)) == 0)
				stations = _map.Stations.OrderBy((z) => DistanceHelper.FindDistance(
				z.Position, robot.Position) - z.Energy - z.RecoveryRate);

			EnergyStation station = stations.First();
			
			foreach (var s in stations)
			{
				if (EnergyAlgorithm.CanCollectEnergy(robot, s, _map))
				{
					station = s;
					break;
				}

				if (HasCollectableFreePositions(s))
				{
					station = s;
					break;
				}
			}

			return station;
		}

		public override bool HasCollectableFreePositions(EnergyStation station)
		{
			var positions = StationHelper.GetCollectablePositions(station, _map);
			positions.RemoveAll((x) => _robots.Any((y) => x == y.Position && y != _robots[_index]));

			if (positions.Count > 0)
				return true;
			return false;
		}

		public Robot.Common.Robot GetAutoritativeRobot()
		{
			var oponents = _robots.Where((x) => x.OwnerName != _robots[_index].OwnerName).
				OrderBy((x) => DistanceHelper.FindDistance(x.Position,
					_robots[_index].Position) - x.Energy);
			if (oponents.Count() == 0)
				return null;
			return oponents.First();
		}
	}
}
