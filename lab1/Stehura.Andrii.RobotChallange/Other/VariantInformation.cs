﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StehuraAndrii.RobotChallange.Other
{
	internal static class VariantInformation
	{
		static public int EnergyToBreed => 1000;

		static public int CollectableArea => 4;

		static public string Author = null;

		static public double StealPercentage => 0.3;

		static public int AttackEnergyWaste => 20;
	}
}
