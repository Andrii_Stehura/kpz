﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DbClient.Models
{
	class ApplicationContext: DbContext
	{
		public ApplicationContext(DbContextOptions<ApplicationContext> options): base(options)
		{
			Database.EnsureCreated();
		}

		public DbSet<House> Houses { get; set; }
		public DbSet<Address> Address { get; set; }
		public DbSet<HouseHolder> HouseHolders { get; set; }
		public DbSet<Income> Income { get; set; }
	}
}
