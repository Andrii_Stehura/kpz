﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DbClient.Models
{
	class HouseHolder
	{
		[Key]
		public int Holder_Num { get; set; }
		public string Holder_Name { get; set; }
		public House House { get; set; }
		public string Holder_Surname { get; set; }
		public int? Flat_Num { get; set; }
		public decimal? Area { get; set; }
	}
}
