﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DbClient.Models
{
	class Address
	{
		[Key]
		public int? Address_Id { get; set; }
		public string Country { get; set; }
		public string Region { get; set; }
		public string City { get; set; }
		public string Street { get; set; }
		public int? Building { get; set; }

	}
}
