﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DbClient.Models
{
	class House
	{
		[Key]
		public int House_Num { get; set; }
		
		public Address Address { get; set; }
	}
}
