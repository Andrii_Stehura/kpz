﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DbClient.Models
{
	class Income
	{
		[Key]
		public int Income_Id { get; set; }
		public decimal Value { get; set; }
		public string Description { get; set; }
		public string Source { get; set; }
		public House House { get; set; }
	}
}
