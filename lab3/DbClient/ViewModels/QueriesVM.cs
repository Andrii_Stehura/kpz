﻿using DbClient.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Linq;

namespace DbClient.ViewModels
{
	class QueriesVM: BaseViewModel
	{
		private string _connectionStr;

		public QueriesVM(string connectionString)
		{
			_connectionStr = connectionString;
			var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
			var options = optionsBuilder.UseSqlServer(_connectionStr).Options;
			var panel = new StackPanel();

			using (ApplicationContext db = new ApplicationContext(options))
			{
				panel.Children.Add(GetTextBlock("Select houses and their addresses:"));
				panel.Children.Add(GetDataGrid(db.Houses
					.Select(x => new
					{
						house_num = x.House_Num,
						address_id = x.Address.Address_Id,
						country = x.Address.Country,
						region = x.Address.Region,
						city = x.Address.City,
						street = x.Address.Street,
						building = x.Address.Building
					}).ToList()));

				panel.Children.Add(GetTextBlock("All cities where country is 'Urkaine':"));
				panel.Children.Add(GetDataGrid(db.Address
					.Where(x => x.Country == "Ukraine")
					.Select(x => new { city = x.City })
					.Distinct()
					.OrderBy(x => x.city)
					.ToList()));

				panel.Children.Add(GetTextBlock("House holders names and surnames grouped by city:"));
				var groupedByCity = db.HouseHolders
					.Select(x => new
					{
						city = x.House.Address.City,
						name = x.Holder_Name,
						surname = x.Holder_Surname
					})
					.ToList()
					.GroupBy(x => x.city)
					.OrderByDescending(x => x.Key);

				ArrayList arrayList = new ArrayList();
				foreach (var values in groupedByCity)
				{
					foreach (var el in values)
						arrayList.Add(new
						{
							city = values.Key,
							name = el.name,
							surname = el.surname
						});
				}
				panel.Children.Add(GetDataGrid(arrayList));

				panel.Children.Add(GetTextBlock("Count how many house holders live in each city:"));
				panel.Children.Add(GetDataGrid(groupedByCity
					.Select(x => new 
					{ 
						city = x.Key,
						count = db.Address.Count(y => y.City == x.Key)
					})));

				panel.Children.Add(GetTextBlock("Sum of house holders flat area living in each city:"));
				panel.Children.Add(GetDataGrid(groupedByCity
					.Select(x => new
					{
						city = x.Key,
						totalArea = db.HouseHolders
							.Where(y => y.House.Address.City == x.Key)
							.Sum(y => y.Area)
					})
					.OrderBy(x=>x.totalArea)
					.ToList()));

				panel.Children.Add(GetTextBlock("House table joined with Income table on houseId:"));
				panel.Children.Add(GetDataGrid(db.Houses
					.Join(db.Income, x => x, y => y.House, (x, y) => new
					{
						houseId = x.House_Num,
						value = y.Value,
						description = y.Description,
						source = y.Source
					}).ToList()));

				panel.Children.Add(GetTextBlock($"Income values ordered and taken while less than 100:"));
				panel.Children.Add(GetDataGrid(db.Income
					.ToList()
					.OrderBy(x => x.Value)
					.TakeWhile(x => x.Value < 100)));

				panel.Children.Add(GetTextBlock($"Income values ordered and skipped while less than 100:"));
				panel.Children.Add(GetDataGrid(db.Income
					.ToList()
					.OrderBy(x => x.Value)
					.SkipWhile(x => x.Value < 100)));

				decimal product = db.Income
					.ToList()
					.TakeWhile((x, i) => i < 10)
					.Select(x => x.Value)
					.Aggregate((x, y) => x * y);

				panel.Children.Add(GetTextBlock($"Product of 10 incomes: {product}"));

				panel.Children.Add(GetTextBlock($"Max income info:"));
				panel.Children.Add(GetDataGrid(db.Income
					.Where(x => x.Value == db.Income.Max(y => y.Value))
					.ToList()));
			}

			DataGrids = panel;
			OnPropertyChanged(nameof(DataGrids));
		}

		private DataGrid GetDataGrid(IEnumerable collection)
		{
			var grid = new DataGrid();
			grid.ItemsSource = collection;
			grid.MaxHeight = 200;
			grid.FontSize = 18;
			return grid;
		}

		private TextBlock GetTextBlock(string text)
		{
			var tb = new TextBlock();
			tb.Text = text;
			tb.Margin = new Thickness(0, 5, 0, 5);
			tb.FontSize = 18;
			return tb;
		}

		public string ConnectionStringText => $"Connection string: {_connectionStr}";

		public UIElement DataGrids { get; set; }
	}
}
