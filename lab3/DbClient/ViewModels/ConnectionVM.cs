﻿using DbClient.Controls;
using DbClient.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Data.SqlClient;
using System.Windows.Controls;
using DbClient.Pages;

namespace DbClient.ViewModels
{
	class ConnectionVM: BaseViewModel
	{
		private string _connectionStr;

		public ConnectionVM()
		{
			ConnectionString = "Data Source = STEG; Initial Catalog = HouseManagmentDB; Integrated Security = True";
		}

		public string ConnectionString
		{
			get => _connectionStr;
			set
			{
				_connectionStr = value;
				OnPropertyChanged();
			}
		}

		public ICommand ConnectCommand => new RelayCommand((x) =>
		{
			if (!IsServerConnected())
			{
				MessageBox.Show($"Cannot connect to database with connection strng: {_connectionStr}",
					"Connection error", MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			if (!_connectionStr.Contains("HouseManagmentDB"))
			{
				MessageBox.Show($"Unknown database", "Connection error", MessageBoxButton.OK,
					MessageBoxImage.Warning);
				return;
			}

			if (Application.Current.MainWindow is MainWindow mw)
			{
				mw.Cursor = Cursors.Wait;
				Page page = new QueriesPage();
				page.DataContext = new QueriesVM(_connectionStr);
				mw.MainFrame.Navigate(page);
				mw.Cursor = Cursors.Arrow;
			}
		});

		private bool IsServerConnected()
		{
			try
			{
				using (var l_oConnection = new SqlConnection(_connectionStr))
				{
					l_oConnection.Open();
					return true;
				}
			}
			catch
			{
				return false;
			}
		}
	}
}
