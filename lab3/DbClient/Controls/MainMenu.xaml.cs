﻿using DbClient.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DbClient.Controls
{
	/// <summary>
	/// Логика взаимодействия для MainMenu.xaml
	/// </summary>
	public partial class MainMenu : UserControl
	{
		public MainMenu()
		{
			InitializeComponent();
		}

		public ICommand ConnectionCommand => new RelayCommand((x)=> 
		{
			if (Application.Current.MainWindow is MainWindow mw)
			{
				var page = new ConnectionPage();
				page.DataContext = new ViewModels.ConnectionVM();
				mw.MainFrame.Navigate(page);
			}
			else
				throw new InvalidCastException(
					$"Cannot cast {Application.Current.MainWindow.GetType()} to {typeof(MainWindow)}");
		});

		public ICommand ExitCommand => new RelayCommand((x) =>
		{
			if (Application.Current.MainWindow is MainWindow mw)
			{
				mw.Close();
			}
			else
				throw new InvalidCastException(
					$"Cannot cast {Application.Current.MainWindow.GetType()} to {typeof(MainWindow)}");
		});
	}
}
