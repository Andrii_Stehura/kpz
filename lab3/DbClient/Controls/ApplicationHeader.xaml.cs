﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DbClient.Controls
{
	/// <summary>
	/// Логика взаимодействия для ApplicationHeader.xaml
	/// </summary>
	public partial class ApplicationHeader : UserControl
	{
		public ApplicationHeader()
		{
			InitializeComponent();
			HeaderText = "Text";
			BackButtonVisibility = Visibility.Hidden;
		}

		public string HeaderText { get; set; }

		public Visibility BackButtonVisibility { get; set; }

		public ICommand BackCommand => new RelayCommand((x) =>
		{
			if (Application.Current.MainWindow is MainWindow mw)
			{
				mw.MainFrame.GoBack();
			}
			else
				throw new InvalidCastException(
					$"Cannot cast {Application.Current.MainWindow.GetType()} to {typeof(MainWindow)}");
		});
	}
}
